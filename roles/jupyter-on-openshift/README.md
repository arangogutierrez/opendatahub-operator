Jupyter On OpenShift
====================

Deploys the JupyterHub and a JupyterHub Postgresql database implemented on the [Jupyter On OpenShift](https://github.com/jupyter-on-openshift/jupyterhub-quickstart) project.

Requirements
------------

None

Role Variables
--------------

* `database_memory` - Sets the memory resource value for the JupyterHub database pod
* `database_password` - Sets the password that is used during the creation of the PostgreSQL JupyterHub database
* `jupyterhub_db_version` - postgresql image version to pull from the openshift repository
* `cookie_secret` - Sets the [JupyterHub Cookie Secret](https://jupyterhub.readthedocs.io/en/stable/getting-started/security-basics.html#cookie-secret) 
* `jupyterhub_memory` - Set the memory resource value for the JupyterHub pod
* `jupyterhub_image` - The standard image used
* `notebook_image` - The notebook image to spawn for each new user
* `notebook_memory` - The memory resource value to use for the notebook pod
* `jupyterhub_config` - A [custom JupyterHub config](https://github.com/jupyter-on-openshift/jupyterhub-quickstart/blob/2a04b902e4930811c1197af3d61ad010b21d04ad/README.md#customising-the-jupyterhub-deployment) that will be added to the JupyterHub pod as the environment variable JUPYTERHUB_CONFIG.

* `extra_env_vars` - Additional environment variables that will be included in the JupyterHub pod deployment. This must be a dictionary with each key:value in the format

```
extra_env_vars:
    env_var_one_name: env_var_one_value
    env_var_two_name: env_var_two_value
```

* `extra_env_vars_secret` - Additional environment variables that will be loaded from secrets and included in the JupyterHub pod deployment. This must be a dictionary with each key:value in the format

```
extra_env_vars_secret:
  secret_name_one:
    name: env_var_one_name
    key: env_var_one_value
  secret_name_two:
    name: env_var_two_name
    key: env_var_two_value
  secret_name_three:
    name: env_var_three_name
    key: env_var_three_value
```

Dependencies
------------

None

Sample Configuration
--------------------

```
jupyter-on-openshift:
  notebook_image: "quay.io/some/image:latest"
  notebook_memory: "1Gi"

  jupyterhub_config: |
    c.KubeSpawner.env_keep = ['env_var_one', 'secret_env_var_one_name']

  extra_env_vars:
      env_var_one_name: env_var_one_value
      env_var_two_name: env_var_two_value
  extra_env_vars_secret:
    secret_name_one:
      name: secret_env_var_one_name
      key: secret_env_var_one_value
    secret_name_two:
      name: secret_env_var_two_name
      key: secret_env_var_two_value
    secret_name_three:
      name: secret_env_var_three_name
      key: secret_env_var_three_value
```

License
-------

GNU GPLv3

Author Information
------------------

contributors@lists.opendatahub.io
